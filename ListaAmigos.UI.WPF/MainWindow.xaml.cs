﻿using ListaAmigos.BIZ;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.DAL.WebService;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ListaAmigos.UI.WPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        AmigoManager _manager;
        string accion = "nuevo";
        string direccionFoto = null;
        public MainWindow()
        {
            InitializeComponent();
            _manager = new AmigoManager(new AmigoRepository());
            ActualizarLista();
            LimpiarCajas();
        }

        private void ActualizarLista(List<AmigoDTO> datos = null)
        {
            lsvAmigos.ItemsSource = null;
            if (datos == null)
            {
                lsvAmigos.ItemsSource = _manager.ObtenerElementos;
            }
            else
            {
                lsvAmigos.ItemsSource = datos;
            }
        }
        private void LimpiarCajas()
        {
            imgFoto.Source = null;
            txbDireccion.Clear();
            txbEmail.Clear();
            txbNombre.Clear();
            txbNotas.Clear();
            txbSobrenombre.Clear();
            txbTelefono.Clear();
            dtpCalendario.SelectedDate = DateTime.Now;
            txtCriterio.Text = "";
            btnCancelar.IsEnabled = false;
            btnEditar.IsEnabled = true;
            btnEliminar.IsEnabled = true;
            btnGuardar.IsEnabled = false;
            btnNuevo.IsEnabled = true;
            habilitarCajas(false);

        }
        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            switch (cmbBuscar.Text)
            {
                case "Nombre":
                    ActualizarLista(_manager.BuscarPorNombre(txtCriterio.Text));
                    break;
                case "Apodo":
                    ActualizarLista(_manager.BuscarPorApodo(txtCriterio.Text));
                    break;
                case "Email":
                    ActualizarLista(_manager.BuscarPorEmail(txtCriterio.Text));
                    break;
                default:
                    ActualizarLista(_manager.BuscarPorTelefono(txtCriterio.Text));
                    break;
            }
        }

        private void btnRestaurar_Click(object sender, RoutedEventArgs e)
        {
            ActualizarLista();
            LimpiarCajas();
        }

        private void btnNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCajas();
            accion = "nuevo";
            btnCancelar.IsEnabled = true;
            btnEditar.IsEnabled = false;
            btnEliminar.IsEnabled = false;
            btnGuardar.IsEnabled = true;
            btnNuevo.IsEnabled = false;
            habilitarCajas(true);
        }

       

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            if (lsvAmigos.SelectedItem != null)
            {
                AmigoDTO a = (AmigoDTO)lsvAmigos.SelectedItem;
                txbDireccion.Text = a.Direccion;
                txbEmail.Text = a.Email;
                txbNombre.Text = a.Nombre;
                txbNotas.Text = a.Notas;
                txbSobrenombre.Text = a.Sobrenombre;
                txbTelefono.Text = a.Telefono;
                dtpCalendario.SelectedDate = a.FechaNacimiento;
                if (a.StreamFoto != null)
                {
                    BitmapImage bmpi = new BitmapImage();
                    bmpi.BeginInit();
                    bmpi.StreamSource = new MemoryStream(a.StreamFoto);
                    bmpi.EndInit();
                    imgFoto.Source = bmpi;
                }
                accion = "editar";
                btnCancelar.IsEnabled = true;
                btnEditar.IsEnabled = false;
                btnEliminar.IsEnabled = false;
                btnGuardar.IsEnabled = true;
                btnNuevo.IsEnabled = false;
                habilitarCajas(true);
            }
        }

        private void btnGuardar_Click(object sender, RoutedEventArgs e)
        {
            AmigoDTO a = new AmigoDTO();
            a.Direccion = txbDireccion.Text;
            a.Email = txbEmail.Text;
            a.FechaNacimiento = (DateTime)dtpCalendario.SelectedDate;
            a.Nombre = txbNombre.Text;
            a.Notas = txbNotas.Text;
            a.Telefono = txbTelefono.Text;
            a.Sobrenombre = txbSobrenombre.Text;
            a.Telefono = txbTelefono.Text;
            a.IdFotografia = direccionFoto;
            if (accion == "nuevo")
            {
                if(MessageBox.Show("¡Realmente deseas agregar este Amigo?","Confirma", MessageBoxButton.YesNo, MessageBoxImage.Question)== MessageBoxResult.Yes)
                {
                    try
                    {
                        a.StreamFoto = ObtenerFoto(a);
                        if (_manager.Agregar(a) != null)
                        {
                            MessageBox.Show("Amigo agregado correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarLista();
                            LimpiarCajas();
                            direccionFoto = "";
                        }
                        else
                        {
                            MessageBox.Show("Tu amigo no se pudo guardar correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            else
            {
                if (MessageBox.Show("¡Realmente deseas modificar este Amigo?", "Confirma", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    try
                    {
                        a.StreamFoto = ObtenerFoto(a);
                        a.Id = ((AmigoDTO)lsvAmigos.SelectedItem).Id;
                        if (_manager.Modificar(a) != null)
                        {
                            MessageBox.Show("Amigo modificado correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
                            ActualizarLista();
                            LimpiarCajas();
                            direccionFoto = "";
                        }
                        else
                        {
                            MessageBox.Show("Tu amigo no se pudo modificar correctamente", "Resultado", MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private byte[] ObtenerFoto(AmigoDTO a)
        {
            if (direccionFoto == null)
            {
                return a.StreamFoto;
            }
            else
            {
                if (direccionFoto != null)
                {
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create(new Uri(direccionFoto)));
                    MemoryStream ms = new MemoryStream();
                    encoder.Save(ms);
                    return ms.ToArray();
                }
                else
                {
                    return a.StreamFoto;
                }
            }
            
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            if (lsvAmigos.SelectedItem != null)
            {
                AmigoDTO a = (AmigoDTO)lsvAmigos.SelectedItem;
                if (MessageBox.Show("Realmente deseas eliminar a " + a.Nombre + "?", "Confirmación", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (_manager.Eliminar((AmigoDTO)lsvAmigos.SelectedItem))
                    {
                        MessageBox.Show("Amigo eliminado correctamente", "Información", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarLista();
                    }
                    else
                    {
                        MessageBox.Show("Error al eliminar al amigo", "Información", MessageBoxButton.OK, MessageBoxImage.Error);
                        ActualizarLista();
                    }
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCajas();
        }

        private void habilitarCajas(bool valor)
        {
            txbDireccion.IsEnabled = valor;
            txbEmail.IsEnabled = valor;
            txbNombre.IsEnabled = valor;
            txbNotas.IsEnabled = valor;
            txbSobrenombre.IsEnabled = valor;
            txbTelefono.IsEnabled = valor;
            dtpCalendario.IsEnabled = valor;
        }

        private void btnBuscarFoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialogo = new OpenFileDialog();
            dialogo.Title = "Buscar fotografía";
            dialogo.Filter = "Imágenes JPG PNG|*.jp;*.png";
            if ((bool)dialogo.ShowDialog())
            {
                imgFoto.Source = new BitmapImage(new Uri(dialogo.FileName));
                direccionFoto = dialogo.FileName;
            }
            else
            {
                direccionFoto = null;
            }
        }
    }
}
