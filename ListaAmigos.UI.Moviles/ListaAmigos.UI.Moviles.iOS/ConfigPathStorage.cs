﻿using System;
using ListaAmigos.COMMON.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(ListaAmigos.UI.Moviles.iOS.ConfigPathStorage))]
namespace ListaAmigos.UI.Moviles.iOS
{
    public class ConfigPathStorage : IConfigPathStorage
    {
        private string _directorioDB;
        public string Path
        {
            get
            {
                if(string.IsNullOrEmpty(_directorioDB))
                {
                    var directorio = System.Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                    _directorioDB = System.IO.Path.Combine(directorio, "..", "Library");
                }
                return _directorioDB;
            }
        }

    }
}
