﻿using ListaAmigos.COMMON.Interfaces;
using Windows.Storage;
using Xamarin.Forms;

[assembly: Dependency(typeof(ListaAmigos.UI.Moviles.UWP.ConfigPathStorage))]
namespace ListaAmigos.UI.Moviles.UWP
{
    public class ConfigPathStorage:IConfigPathStorage
    {
        private string _directorioDB;
        public string Path
        {
            get
            {
                if (string.IsNullOrEmpty(_directorioDB))
                {
                    _directorioDB = ApplicationData.Current.LocalFolder.Path+"\\";
                }
                return _directorioDB;
            }
        }
    }
}
