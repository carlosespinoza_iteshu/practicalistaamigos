using ListaAmigos.COMMON.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(ListaAmigos.UI.Moviles.Droid.ConfigPathStorage))]
namespace ListaAmigos.UI.Moviles.Droid
{
    public class ConfigPathStorage : IConfigPathStorage
    {
        private string _directorioDB;
        public string Path
        {
            get
            {
                if(string.IsNullOrEmpty(_directorioDB))
                {
                    _directorioDB = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                }
                return _directorioDB;
            }
        }
    }
}