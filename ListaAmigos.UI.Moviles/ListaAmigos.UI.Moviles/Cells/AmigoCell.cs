﻿using Xamarin.Forms;

namespace ListaAmigos.UI.Moviles.Cells
{
    public class AmigoCell: ViewCell
    {
        public AmigoCell()
        {
            //primero crearemos todos los objetos
            Image imgFoto = new Image();
            imgFoto.SetBinding(Image.SourceProperty, new Binding("StreamFoto"));
            Label lblNombre = new Label()
            {
                FontAttributes = FontAttributes.Bold,
               
            };
            lblNombre.SetBinding(Label.TextProperty, new Binding("Nombre"));
            Label lblParentesis1 = new Label()
            {
                FontAttributes = FontAttributes.Bold,
                Text = " ("
            };
            Label lblSobrenombre = new Label()
            {
                FontAttributes = FontAttributes.Bold
            };
            Label lblParentesis2 = new Label()
            {
                FontAttributes = FontAttributes.Bold,
                Text = ")"
            };
            lblSobrenombre.SetBinding(Label.TextProperty, new Binding("Sobrenombre"));
            Image imgCel = new Image()
            {
                Source = "telefono.png"
            };
            Label lblTelefono = new Label();
            lblTelefono.SetBinding(Label.TextProperty, new Binding("Telefono"));
            Image imgCorreo = new Image()
            {
                Source = "email.png"
            };
            Label lblEmail = new Label();
            lblEmail.SetBinding(Label.TextProperty, new Binding("Email"));
            Image imgMapa = new Image()
            {
                Source = "mapa.png"
            };
            Label lblDireccion = new Label();
            lblDireccion.SetBinding(Label.TextProperty, new Binding("Direccion"));


            //Ahora creamos el grid; pero ahora lo realizaremos con stacks
            StackLayout renglonInferior = new StackLayout();
            renglonInferior.Orientation = StackOrientation.Horizontal;
            renglonInferior.Children.Add(imgMapa);
            renglonInferior.Children.Add(lblDireccion);
            StackLayout renglonMedio = new StackLayout();
            renglonMedio.Orientation = StackOrientation.Horizontal;
            renglonMedio.Children.Add(imgCel);
            renglonMedio.Children.Add(lblTelefono);
            renglonMedio.Children.Add(imgCorreo);
            renglonMedio.Children.Add(lblEmail);
            StackLayout renglonSuperior = new StackLayout();
            renglonSuperior.Orientation = StackOrientation.Horizontal;
            renglonSuperior.Children.Add(lblNombre);
            renglonSuperior.Children.Add(lblParentesis1);
            renglonSuperior.Children.Add(lblSobrenombre);
            renglonSuperior.Children.Add(lblParentesis2);
            StackLayout stackRenglones = new StackLayout();
            stackRenglones.Children.Add(renglonSuperior);
            stackRenglones.Children.Add(renglonMedio);
            stackRenglones.Children.Add(renglonInferior);
            StackLayout stackItem = new StackLayout();
            stackItem.Orientation = StackOrientation.Horizontal;
            stackItem.Children.Add(imgFoto);
            stackItem.Children.Add(stackRenglones);
            //por ultimo asignamos nuestro stack a la vista
            View = stackItem;
        }
        
    }
}
