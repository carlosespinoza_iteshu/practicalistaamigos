﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using ListaAmigos.BIZ;
using ListaAmigos.DAL.WebService;
using ListaAmigos.UI.Moviles.Cells;

namespace ListaAmigos.UI.Moviles
{
    public partial class MainPage : ContentPage
    {
        private AmigoManager _manager;
        public MainPage()
        {
            InitializeComponent();
            Padding = Device.OnPlatform<Thickness>(new Thickness(0, 20, 0, 0),
                                                    new Thickness(0),
                                                    new Thickness(0));
            pckBuscarPor.Items.Add("Por Nombre");
            pckBuscarPor.Items.Add("Por Apodo");
            pckBuscarPor.Items.Add("Por Email");
            pckBuscarPor.Items.Add("Por Teléfono");
            var config = DependencyService.Get<IConfigPathStorage>();
            _manager = new AmigoManager(new AmigoRepository(config.Path));
            lsvAmigos.ItemTemplate = new DataTemplate(typeof(AmigoCell));
            ActualizarLista();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            ActualizarLista();
        }

        private void ActualizarLista(List<AmigoDTO> datos=null)
        {
            lsvAmigos.ItemsSource = null;
            if (datos == null)
            {
                lsvAmigos.ItemsSource = _manager.ObtenerElementos;
            }
            else
            {
                lsvAmigos.ItemsSource = datos;
            }
        }

        private void btnBuscar_Clicked(object sender, EventArgs e)
        {
            switch (pckBuscarPor.SelectedIndex)
            {
                case 0:
                    ActualizarLista(_manager.BuscarPorNombre(entCriterio.Text));
                    break;
                case 1:
                    ActualizarLista(_manager.BuscarPorApodo(entCriterio.Text));
                    break;
                case 2:
                    ActualizarLista(_manager.BuscarPorEmail(entCriterio.Text));
                    break;
                case 3:
                    ActualizarLista(_manager.BuscarPorTelefono(entCriterio.Text));
                    break;
                default:
                    break;
            }
        }

        private void btnRestaurar_Clicked(object sender, EventArgs e)
        {
            ActualizarLista();
        }

        private void btnNuevo_Clicked(object sender, EventArgs e)
        {
            Navigation.PushModalAsync(new EditorPage());
        }

        private void lsvAmigos_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Navigation.PushModalAsync(new EditorPage(e.Item as AmigoDTO));
        }
    }
}
