﻿using ListaAmigos.BIZ;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using ListaAmigos.DAL.WebService;
using System;
using Xamarin.Forms;

namespace ListaAmigos.UI.Moviles
{
    public partial class EditorPage : ContentPage
    {
        private AmigoManager _amigoManager;
        private AmigoDTO _amigo;
        private string accion;
        public EditorPage(AmigoDTO item=null)
        {
            InitializeComponent();
            Padding = Device.OnPlatform<Thickness>(new Thickness(0, 20, 0, 0),
                                                    new Thickness(0),
                                                    new Thickness(0));
            var config = DependencyService.Get<IConfigPathStorage>();
            _amigoManager = new AmigoManager(new AmigoRepository(config.Path));
            if (item==null)
            {
                //nuevo
                accion = "nuevo";
                btnEliminar.IsEnabled = false;
                _amigo = new AmigoDTO();
            }
            else
            {
                //editar
                accion = "editar";
                btnEliminar.IsEnabled = true;
                _amigo = item;
                entNombre.Text = _amigo.Nombre;
                entSobrenombre.Text = _amigo.Sobrenombre;
                entTelefono.Text = _amigo.Telefono;
                dpkFechaNacimiento.Date = _amigo.FechaNacimiento;
                entEmail.Text = _amigo.Email;
                entDireccion.Text = _amigo.Direccion;
                entNotas.Text = _amigo.Notas;
            }
            
        }

        private async void btnGuardar_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entNombre.Text))
            {
                await DisplayAlert("Error:", "El nombre esta vacío", "Ok");
                entNombre.Focus();
                return;
            }

            bool r = await DisplayAlert("Lista amigos", "¿Realmente deseas guardar la información de este amigo?", "Sí", "No");
            if (r)
            {
                _amigo.Nombre = entNombre.Text;
                _amigo.Sobrenombre = entSobrenombre.Text;
                _amigo.Telefono = entTelefono.Text;
                _amigo.FechaNacimiento = dpkFechaNacimiento.Date;
                _amigo.Email = entEmail.Text;
                _amigo.Direccion = entDireccion.Text;
                _amigo.Notas = entNotas.Text;
                if (accion == "nuevo")
                {
                    
                    _amigoManager.Agregar(_amigo);

                    await DisplayAlert("Lista Amigos", "Amigo guardado correctamente", "Ok");
                    await Navigation.PopModalAsync();
                }
                else
                {
                    _amigoManager.Modificar(_amigo);

                    await DisplayAlert("Lista Amigos", "Amigo modificado correctamente", "Ok");
                    await Navigation.PopModalAsync();

                }
            }
        }

        private void btnCancelar_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private async void btnEliminar_Clicked(object sender, EventArgs e)
        {
            bool r = await DisplayAlert("Lista amigos", "¿Realmente deseas eliminar este amigo?", "Sí", "No");
            if (r)
            {
                if (_amigoManager.Eliminar(_amigo))
                {
                    await DisplayAlert("Lista Amigos", "Amigo eliminado correctamente", "Ok");
                    await Navigation.PopModalAsync();
                }
                else
                {
                    await DisplayAlert("Lista Amigos", "A ocurrido un error...", "Ok");
                }
            }
        }
    }
}
