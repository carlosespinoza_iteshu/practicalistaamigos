﻿using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using System.Collections.Generic;

namespace ListaAmigos.DAL.WebService
{
    public class AmigoRepository : IGenericRepository<AmigoDTO>
    {
        RestService<AmigoDTO> _ws;
        public AmigoRepository(string path = null)
        {
            //le dejamos el path simplemente para hacerla totalmente compatible con la de la otra DAL, pero no se va a usar realmente.
            _ws = new RestService<AmigoDTO>("http://servicioiteshu.somee.com/", "api/amigos/");
        }

        public List<AmigoDTO> ReadItems
        {
            get
            {
                return _ws.ObtenerDatos().Result;
            }

            set
            {
                
            }
        }

        public AmigoDTO Create(AmigoDTO entidad)
        {
            return _ws.Guardar(entidad).Result;
        }

        public bool Delete(AmigoDTO entidad)
        {
            return _ws.Eliminar(entidad, entidad.Id).Result;
        }

        public AmigoDTO Update(AmigoDTO entidad)
        {
            return _ws.Actualizar(entidad).Result;
        }
    }
}
