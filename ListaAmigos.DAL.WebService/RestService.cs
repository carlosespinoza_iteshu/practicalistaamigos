﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ListaAmigos.DAL.WebService
{
    public class RestService<T> where T:class
    {
        private HttpClient _cliente;
        private string _uriApi;
        public RestService(string uriBase,string uriApi)
        {
            _uriApi = uriApi;
            _cliente = new HttpClient();
            _cliente.BaseAddress = new Uri(uriBase);
            _cliente.DefaultRequestHeaders.Accept.Clear();
            _cliente.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _cliente.MaxResponseContentBufferSize = 256000;
        }

        public async Task<List<T>> ObtenerDatos()
        {
            HttpResponseMessage response = await _cliente.GetAsync(_uriApi).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var items = JsonConvert.DeserializeObject<List<T>>(content);
                return items;
            }
            else
            {
                return null;
            }
        }

        public async Task<T> Guardar(T entidad)
        {
            var json = JsonConvert.SerializeObject(entidad);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await _cliente.PostAsync(_uriApi, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(respuesta);
                return item;
            }
            else
            {
                return null;
            }
        }

        public async Task<T> Actualizar(T entidad)
        {
            var json = JsonConvert.SerializeObject(entidad);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await _cliente.PutAsync(_uriApi, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(respuesta);
                return item;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> Eliminar(T entidad, string id)
        {
            var json = JsonConvert.SerializeObject(entidad);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await _cliente.DeleteAsync(string.Format("{0}{1}", _uriApi, id)).ConfigureAwait(false);
            return response.IsSuccessStatusCode;
        }
        
    }
}
