﻿using ListaAmigos.COMMON.Entidades;
using System.Collections.Generic;

namespace ListaAmigos.COMMON.Interfaces
{
    /// <summary>
    /// Interfase para implementar la lógica de negocio de la entidad Amigo, Debe ser implementada en la capa DAL
    /// </summary>
    public interface IAmigoManager:IGenericManager<AmigoDTO>
    {
        /// <summary>
        /// Obtiene la lista de Amigos que coinciden con el nombre dado
        /// </summary>
        /// <param name="nombre">Criterio de búsqueda</param>
        /// <returns>Lista de amigos</returns>
        List<AmigoDTO> BuscarPorNombre(string nombre);
        /// <summary>
        /// Obtiene la lista de Amigos que coinciden con el apodo dado
        /// </summary>
        /// <param name="nombre">Criterio de búsqueda</param>
        /// <returns>Lista de amigos</returns>
        List<AmigoDTO> BuscarPorApodo(string apodo);
        /// <summary>
        /// Obtiene la lista de Amigos que coinciden con el Email dado
        /// </summary>
        /// <param name="nombre">Criterio de búsqueda</param>
        /// <returns>Lista de amigos</returns>
        List<AmigoDTO> BuscarPorEmail(string email);
        /// <summary>
        /// Obtiene la lista de Amigos que coinciden con el telefono dado
        /// </summary>
        /// <param name="nombre">Criterio de búsqueda</param>
        /// <returns>Lista de amigos</returns>
        List<AmigoDTO> BuscarPorTelefono(string telefono);
    }
}
