﻿namespace ListaAmigos.COMMON.Interfaces
{
    public interface IConfigPathStorage
    {
        string Path { get;  }
    }
}
