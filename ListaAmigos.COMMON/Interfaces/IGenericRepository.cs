﻿using System.Collections.Generic;

namespace ListaAmigos.COMMON.Interfaces
{
    /// <summary>
    /// Interfaz para implementar los métodos CRUD (Create, Read, Update, Delete) de una entidad; esta deberá ser implementada en la capa DAL del proyecto.
    /// </summary>
    /// <typeparam name="T">Tipo de entidad del la cual se desea implementar el CRUD, regularmente es un DTO</typeparam>
    public interface IGenericRepository<T> where T: class
    {
        T Create(T entidad);
        List<T> ReadItems { get; set; }
        T Update(T entidad);
        bool Delete(T entidad);
    }
}
