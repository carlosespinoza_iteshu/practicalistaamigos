﻿using System.Collections.Generic;

namespace ListaAmigos.COMMON.Interfaces
{
    /// <summary>
    /// Interfaz que permite implementar las llamadas a los metodos CRUD de la capa DAL, esta interfaz debe implementarse en la capa BIZ del proyecto, en todos los objetos Manager.
    /// </summary>
    /// <typeparam name="T">Tipo de entidad DTO</typeparam>
    public interface IGenericManager<T> where T: class
    {
        /// <summary>
        /// Agrega un elemento al repositorio
        /// </summary>
        /// <param name="entidad">Elemento a agregar</param>
        /// <returns>Elemento agregado</returns>
        T Agregar(T entidad);
        /// <summary>
        /// Obtiene la lista de elementos en el repositorio
        /// </summary>
        List<T> ObtenerElementos { get; set; }
        /// <summary>
        /// Modifica el elemento proporcionado en el repositorio
        /// </summary>
        /// <param name="entidad">Entidad a modificar</param>
        /// <returns>Entidad modificada</returns>
        T Modificar(T entidad);
        /// <summary>
        /// Elimina una entidad del repositorio
        /// </summary>
        /// <param name="entidad">Entidad a eliminar</param>
        /// <returns>Indica si la entidad pudo ser eliminada o no</returns>
        bool Eliminar(T entidad);
    }
}
