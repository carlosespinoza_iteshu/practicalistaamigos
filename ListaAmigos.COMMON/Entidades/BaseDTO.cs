﻿using System;

namespace ListaAmigos.COMMON.Entidades
{
    /// <summary>
    /// Clase que permite eliminar los recursos de forma eficiente implementado la interfaz IDisposable; De esta clase deberán heredar todas las entidades del sistema que se almacenen. 
    /// </summary>
    public abstract class BaseDTO: IDisposable
    {
        private bool _isDisposed;
        public void Dispose()
        {
            if (!_isDisposed)
            {
                this._isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
    }
}
