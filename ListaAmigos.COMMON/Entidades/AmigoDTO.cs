﻿using System;

namespace ListaAmigos.COMMON.Entidades
{
    public class AmigoDTO: BaseDTO
    {
        public string Id { get; set; }
        public string Nombre { get; set; }
        public string Sobrenombre { get; set; }
        public string Telefono { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Email { get; set; }
        public string Direccion { get; set; }
        public string Notas { get; set; }
        public string IdFotografia { get; set; }
        public byte[] StreamFoto { get; set; }
    }
}
