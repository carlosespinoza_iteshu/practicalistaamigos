﻿using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using LiteDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ListaAmigos.DAL.NoSQL.LOCAL
{
    /// <summary>
    /// Clase que permite almacenar y recuperar los datos de amigos en una tabla NoSQL local mediante la herramienta LiteDB
    /// </summary>
    public class AmigoRepository : IGenericRepository<AmigoDTO>
    {
        private string _dbName = "Amigos.db";
        private string _tableName = "amigos";
        public AmigoRepository(string path=null)
        {
            if (path != null)
            {
                _dbName = path + _dbName;
            }
        }
        /// <summary>
        /// Obtiene la lista de Amigos almacenados
        /// </summary>
        public List<AmigoDTO> ReadItems
        {
            get
            {
                List<AmigoDTO> datos;
                using (var db = new LiteDatabase(_dbName))
                {
                    datos= db.GetCollection<AmigoDTO>(_tableName).FindAll().ToList<AmigoDTO>();
                }
                return datos;
            }

            set
            {
                
            }
        }
        /// <summary>
        /// Inserta un amigo en la tabla NoSQL
        /// </summary>
        /// <param name="entidad">Amigo a insertar, el campo ID debería estar vació</param>
        /// <returns>Entidad AmigoDTO completa</returns>
        public AmigoDTO Create(AmigoDTO entidad)
        {
            //Si por ejemplo la entidad debe incrementar el ID o algo mas, es justo aqui donde se debe de hacer.

            entidad.Id = Guid.NewGuid().ToString();
            
            using (var db = new LiteDatabase(_dbName))
            {
                var colection = db.GetCollection<AmigoDTO>(_tableName);
                if(entidad.IdFotografia!=null)
                {
                    db.FileStorage.Upload(entidad.Id, entidad.IdFotografia, new MemoryStream(entidad.StreamFoto));
                    entidad.IdFotografia = entidad.Id;
                }
                colection.Insert(entidad.Id,entidad);
            }
            return entidad;
        }
       

        /// <summary>
        /// Elimina un Amigo de la tabla NoSQL
        /// </summary>
        /// <param name="entidad">Amigo a eliminar</param>
        /// <returns>Si pudo o no eliminar al amigo</returns>
        public bool Delete(AmigoDTO entidad)
        {
            int r;
            using (var db = new LiteDatabase(_dbName))
            {
                if (entidad.IdFotografia != null)
                {
                    db.FileStorage.Delete(entidad.IdFotografia);
                }
                var colection = db.GetCollection<AmigoDTO>(_tableName);
                r= colection.Delete(e => e.Id == entidad.Id);
            }
            return r > 0 ? true : false;
        }
        /// <summary>
        /// Actualiza un amigo, no deberia cambiar el campo Id, ya que en base a este es como se buscara
        /// </summary>
        /// <param name="entidad">Amigo con los datos a modificar</param>
        /// <returns>Amigo ya modificado</returns>
        public AmigoDTO Update(AmigoDTO entidad)
        {
            bool r;
            using (var db = new LiteDatabase(_dbName))
            {
                var colection = db.GetCollection<AmigoDTO>(_tableName);
                db.FileStorage.Delete(entidad.Id);
                if (entidad.StreamFoto != null)
                {
                    db.FileStorage.Upload(entidad.Id, entidad.IdFotografia,new MemoryStream(entidad.StreamFoto));
                }
                entidad.IdFotografia = entidad.Id;
                r=colection.Update(entidad);
            }
            if (r)
            {
                return entidad;
            }
            else
            {
                return null;
            }
        }
    }
}
