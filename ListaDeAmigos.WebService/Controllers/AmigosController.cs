﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.DAL.NoSQL.LOCAL;
using System;

namespace ListaDeAmigos.WebService.Controllers
{
    public class AmigosController : ApiController
    {
        AmigoRepository _repositorio;
        public AmigosController()
        {
            _repositorio = new AmigoRepository(@"d:\DZHosts\LocalUser\profecarlos\Protected.servicioiteshu.somee.com\");
        }
        // GET: api/Amigos
        public IEnumerable<AmigoDTO> Get()
        {
            return _repositorio.ReadItems;
        }

        // GET: api/Amigos/5
        public AmigoDTO Get(string id)
        {
            return _repositorio.ReadItems.Where(a => a.Id == id).SingleOrDefault();
        }

        // POST: api/Amigos
        public AmigoDTO Post([FromBody]AmigoDTO value)
        {
            return _repositorio.Create(value);
        }

        // PUT: api/Amigos/5
        public AmigoDTO Put([FromBody]AmigoDTO value)
        {
            return _repositorio.Update(value);
        }

        // DELETE: api/Amigos/5
        public bool Delete(string id)
        {
            return _repositorio.Delete(_repositorio.ReadItems.Where(a => a.Id == id).SingleOrDefault());
        }
    }
}
