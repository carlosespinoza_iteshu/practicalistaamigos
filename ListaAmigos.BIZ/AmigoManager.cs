﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;

namespace ListaAmigos.BIZ
{
    public class AmigoManager : IAmigoManager
    {
        private IGenericRepository<AmigoDTO> _repositorio;
        /// <summary>
        /// Crea una instancia de AmigoManager, regularmente se usa mediante implementación de la interfaz IAmigoManager, recibe como parámetro un objeto de tipo IGenericRepository
        /// </summary>
        /// <example>
        /// IAmigoManager _manager=new AmigoManager(new AmigoRepository());
        /// </example>
        /// <param name="repositorio"></param>
        public AmigoManager(IGenericRepository<AmigoDTO> repositorio)
        {
            _repositorio = repositorio;
        }
        /// <summary>
        /// Recupera la lista de Amigos
        /// </summary>
        public List<AmigoDTO> ObtenerElementos
        {
            get
            {
                return _repositorio.ReadItems;
            }

            set
            {
            }
        }
        /// <summary>
        /// Agrega un Amigo al repositorio
        /// </summary>
        /// <param name="entidad">Amigo a agregar, regularmente el parámetro Id debería venir vacío</param>
        /// <returns>Entidad completa, con Id generado</returns>
        public AmigoDTO Agregar(AmigoDTO entidad)
        {
            return _repositorio.Create(entidad);
        }
        /// <summary>
        /// Busca Amigos que coincida con el Apodo dado.
        /// </summary>
        /// <param name="apodo">Criterio de búsqueda</param>
        /// <returns>Lista de Amigos que cumplen con el criterio de búsqueda</returns>
        public List<AmigoDTO> BuscarPorApodo(string apodo)
        {
            return ObtenerElementos.Where(a => a.Sobrenombre.Contains(apodo)).ToList();
        }
        /// <summary>
        /// Busca Amigos que coincida con el Apodo dado.
        /// </summary>
        /// <param name="email">Criterio de búsqueda</param>
        /// <returns>Lista de Amigos que cumplen con el criterio de búsqueda</returns>
        public List<AmigoDTO> BuscarPorEmail(string email)
        {
            return ObtenerElementos.Where(a => a.Email.Contains(email)).ToList();
        }
        /// <summary>
        /// Busca Amigos que coincida con el Nombre dado.
        /// </summary>
        /// <param name="nombre">Criterio de búsqueda</param>
        /// <returns>Lista de Amigos que cumplen con el criterio de búsqueda</returns>
        public List<AmigoDTO> BuscarPorNombre(string nombre)
        {
            return ObtenerElementos.Where(a => a.Nombre.Contains(nombre)).ToList();
        }
        /// <summary>
        /// Busca Amigos que coincida con el Teléfono dado.
        /// </summary>
        /// <param name="telefono">Criterio de búsqueda</param>
        /// <returns>Lista de Amigos que cumplen con el criterio de búsqueda</returns>
        public List<AmigoDTO> BuscarPorTelefono(string telefono)
        {
            return ObtenerElementos.Where(a => a.Telefono.Contains(telefono)).ToList();
        }
        /// <summary>
        /// Elimina un amigo del repositorio
        /// </summary>
        /// <param name="entidad">Amigo a eliminar</param>
        /// <returns>Si pudo o no eliminar al Amigo</returns>
        public bool Eliminar(AmigoDTO entidad)
        {
            return _repositorio.Delete(entidad);
        }
        /// <summary>
        /// Modifica los datos de un Amigo
        /// </summary>
        /// <param name="entidad">Amigo a modificar, ya deberá contener los datos modificados</param>
        /// <returns>Amigo modificado</returns>
        public AmigoDTO Modificar(AmigoDTO entidad)
        {
            return _repositorio.Update(entidad);
        }
    }
}
