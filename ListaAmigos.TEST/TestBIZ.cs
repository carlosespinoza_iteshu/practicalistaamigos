﻿using ListaAmigos.BIZ;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.COMMON.Interfaces;
using ListaAmigos.DAL.NoSQL.LOCAL;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ListaAmigos.TEST
{
    [TestClass]
    public class TestBIZ
    {
        IAmigoManager _manager = new AmigoManager(new AmigoRepository());
        string notas = "Registro de prueba " + Guid.NewGuid().ToString();
        private void CrearAmigos()
        {
            //Crear 5 datos iguales
            AmigoDTO amigo;
            for (int i = 0; i < 5; i++)
            {
                amigo = new AmigoDTO()
                {
                    Direccion = "Prueba",
                    Email = "Prueba@test.com",
                    FechaNacimiento = DateTime.Now,
                    Nombre = "Registro de prueba",
                    Notas = notas,
                    Sobrenombre = "Apodo de Prueba",
                    Telefono = "1234567890"
                };
                _manager.Agregar(amigo);
            }
            
        }

        private void EliminarAmigos()
        {
            List<AmigoDTO> datos = _manager.ObtenerElementos.Where(a => a.Notas == notas).ToList();
            foreach (var item in datos)
            {
                _manager.Eliminar(item);
            }
        }

        [TestMethod]
        public void TestBuscarPorNombre()
        {
            CrearAmigos();
            int cantidad = _manager.BuscarPorNombre("Registro de prueba").Count;
            EliminarAmigos();
            Assert.AreEqual(5, cantidad, "Se obtuvo una cantidad diferente a 5 registros");
        }

        [TestMethod]
        public void TestBuscarPorApodo()
        {
            CrearAmigos();
            int cantidad = _manager.BuscarPorApodo("Apodo de Prueba").Count;
            EliminarAmigos();
            Assert.AreEqual(5, cantidad, "Se obtuvo una cantidad diferente a 5 registros");
        }

        [TestMethod]
        public void TestBuscarPorTelefono()
        {
            CrearAmigos();
            int cantidad = _manager.BuscarPorTelefono("1234567890").Count;
            EliminarAmigos();
            Assert.AreEqual(5, cantidad, "Se obtuvo una cantidad diferente a 5 registros");
        }

        [TestMethod]
        public void TestBuscarPorEmail()
        {
            CrearAmigos();
            int cantidad = _manager.BuscarPorEmail("Prueba@test.com").Count;
            EliminarAmigos();
            Assert.AreEqual(5, cantidad, "Se obtuvo una cantidad diferente a 5 registros");
        }
    }
}
