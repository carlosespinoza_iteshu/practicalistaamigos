﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ListaAmigos.COMMON.Entidades;
using ListaAmigos.DAL.NoSQL.LOCAL;

namespace ListaAmigos.TEST
{

    [TestClass]
    public class TestDALNoSqlLocal
    {
        [TestMethod]
        public void AgregaAlumno()
        {
            AmigoDTO a = new AmigoDTO();
            a.Nombre = Guid.NewGuid().ToString();
            a.Sobrenombre = "Test";
            a.Direccion = "Dirección de prueba";
            a.Email = "prueba@test.com";
            a.FechaNacimiento = DateTime.Now;
            a.Notas = "notas";
            a.Telefono = "0123456789";

            AmigoRepository repository = new AmigoRepository();
            repository.Create(a);
            List<AmigoDTO> datos = repository.ReadItems;
            Assert.AreEqual(datos.Count(d => d.Nombre == a.Nombre), 1, "No se encontró amigo que coincidiera con el valor esperado");
            repository.Delete(a);
        }

        [TestMethod]
        public void ModificarAlumno()
        {
            AmigoDTO a = new AmigoDTO();
            a.Nombre = Guid.NewGuid().ToString();
            a.Sobrenombre = "Test";
            a.Direccion = "Dirección de prueba";
            a.Email = "prueba@test.com";
            a.FechaNacimiento = DateTime.Now;
            a.Notas = "notas";
            a.Telefono = "0123456789";

            AmigoRepository repository = new AmigoRepository();
            repository.Create(a);

            a.Telefono = "987654321";
            AmigoDTO r=repository.Update(a);
            Assert.AreEqual("987654321",r.Telefono,"El amigo no se actualizo");
            repository.Delete(r);
        }

        [TestMethod]
        public void EliminarAmigo()
        {
            string nombre= Guid.NewGuid().ToString();
            AmigoDTO a = new AmigoDTO();
            a.Nombre = nombre;
            a.Sobrenombre = "Test";
            a.Direccion = "Dirección de prueba";
            a.Email = "prueba@test.com";
            a.FechaNacimiento = DateTime.Now;
            a.Notas = "notas";
            a.Telefono = "0123456789";
            AmigoRepository repository = new AmigoRepository();
            repository.Create(a);
            repository.Delete(a);
            List<AmigoDTO> datos = repository.ReadItems;
            Assert.AreEqual(datos.Count(d => d.Nombre == nombre), 0, "Se encontró amigo que coincidiera con el valor esperado");
        }
    }
}
